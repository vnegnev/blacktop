#Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ ipgui::add_page $IPINST  -name "Page 0" -layout vertical]
	set Component_Name [ ipgui::add_param  $IPINST  -parent  $Page0  -name Component_Name ]
	set ERROR_LENGTH [ipgui::add_param $IPINST -parent $Page0 -name ERROR_LENGTH]
	set PACKET_LENGTH [ipgui::add_param $IPINST -parent $Page0 -name PACKET_LENGTH]
}

proc update_PARAM_VALUE.ERROR_LENGTH { PARAM_VALUE.ERROR_LENGTH } {
	# Procedure called to update ERROR_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ERROR_LENGTH { PARAM_VALUE.ERROR_LENGTH } {
	# Procedure called to validate ERROR_LENGTH
	return true
}

proc update_PARAM_VALUE.PACKET_LENGTH { PARAM_VALUE.PACKET_LENGTH } {
	# Procedure called to update PACKET_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PACKET_LENGTH { PARAM_VALUE.PACKET_LENGTH } {
	# Procedure called to validate PACKET_LENGTH
	return true
}


proc update_MODELPARAM_VALUE.PACKET_LENGTH { MODELPARAM_VALUE.PACKET_LENGTH PARAM_VALUE.PACKET_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PACKET_LENGTH}] ${MODELPARAM_VALUE.PACKET_LENGTH}
}

proc update_MODELPARAM_VALUE.ERROR_LENGTH { MODELPARAM_VALUE.ERROR_LENGTH PARAM_VALUE.ERROR_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ERROR_LENGTH}] ${MODELPARAM_VALUE.ERROR_LENGTH}
}

