//`timescale 1ns / 1ps

`ifndef _RX_
 `define _RX_

module rx(
	  input 			clk_c_i,
   
	  //whether transmission was successful, plus valid
	  //msb holds whether a or b is concerned
	  output reg 			ok_valid_s_o,
	  output reg [2-1:0] 		ok_d_o,

	  //error flags for error register, plus valid
	  output reg 			error_valid_s_o,
	  output reg [SLAVE_ERROR_LENGTH+2+2-1:0] error_d_o, // 2 bits added by this core

	  //receive line
	  input 			rx_d_i
	  );
   
   //length of error flags from slave, non-comms
   parameter SLAVE_ERROR_LENGTH = 10;

   // length of error flags from slave non-comms, plus error flags from slave comms core
   localparam SLAVE_TOT_ERROR_LENGTH = SLAVE_ERROR_LENGTH+2;
   localparam XOR_ERROR_BIT = SLAVE_TOT_ERROR_LENGTH;
   localparam NO_STOP_ERROR_BIT = SLAVE_TOT_ERROR_LENGTH + 1;
   
   //register that holds the ongoing xor value
   reg 					xor_s = 1'b0;

   // complete error word received from slave
   reg [SLAVE_TOT_ERROR_LENGTH-1:0] 		slave_word_d = 0; 
   
   reg [3-1:0] 				hold_ok = 0;
   
   //counter (multi-purpose)
   reg [6-1:0] 				cnt_s = 0;

   // Pipeline/synch reg
   reg 					rx_d_r = 1, rx_d = 1;

   //// FSM
   
   localparam RECV_IDLE = 'h1,
     RECV_OK_A = 'h2,
     RECV_OK_B = 'h4,
     RECV = 'h8,
     RECV_PARITY = 'h10,
     RECV_STOP = 'h20
			  ;
   reg [6-1:0] 				state = RECV_IDLE;
   
   initial begin
      ok_valid_s_o <= 1'b0;
      error_valid_s_o <= 1'b0;
      error_d_o <= 0;
      ok_d_o <= 0;
   end
   
   always @(posedge clk_c_i) begin
      {rx_d, rx_d_r} <= {rx_d_r, rx_d_i};
      
      //defaults
      ok_valid_s_o <= 1'b0;
      error_valid_s_o <= 1'b0;
      
      case(state)
        
        RECV_IDLE:
	  begin
	     if (rx_d==1'b0) begin
		//initialize everything
		xor_s <= 1'b0;
		cnt_s <= 0;
		error_d_o <= 0;
		state <= RECV_OK_A;
	     end
	  end
	
	RECV_OK_A:
	  begin
	     hold_ok <= {hold_ok,rx_d};
	     
	     //ok is 2x 3 bits long
	     cnt_s <= cnt_s + 1;
	     if ( cnt_s == (3-1) ) begin
		if ( (hold_ok==3'b000) || (hold_ok==3'b001) || (hold_ok==3'b010) || (hold_ok==3'b100) )
		  ok_d_o[1] <= 1'b0;
		else
		  ok_d_o[1] <= 1'b1;
		
		cnt_s <= 0;
		state <= RECV_OK_B;
	     end
	  end
	
	RECV_OK_B:
	  begin
	     hold_ok <= {hold_ok,rx_d};
	     
	     //ok is 2x 3 bits long
	     cnt_s <= cnt_s + 1;
	     if ( cnt_s == (3-1) ) begin
		if ( (hold_ok==3'b000) || (hold_ok==3'b001) || (hold_ok==3'b010) || (hold_ok==3'b100) )
		  ok_d_o[0] <= 1'b0;
		else
		  ok_d_o[0] <= 1'b1;
		ok_valid_s_o <= 1'b1;
		
		cnt_s <= 0;
		state <= RECV;
	     end
	  end
	
	RECV:
	  begin
	     //receive word and generate xor
	     slave_word_d <= {slave_word_d, rx_d};
	     xor_s <= xor_s ^ rx_d;
	     
	     //transmission is SLAVE_TOT_ERROR_LENGTH bits long
	     cnt_s <= cnt_s + 1;
	     if ( cnt_s == (SLAVE_TOT_ERROR_LENGTH-1) ) begin
		state <= RECV_PARITY;
	     end
	  end
	
	RECV_PARITY:
	  begin
	     //if incoming bit is not equal to the inverted xor -> error
	     if (rx_d != !xor_s) error_d_o[XOR_ERROR_BIT] <= 1'd1;
	     
	     error_d_o[SLAVE_TOT_ERROR_LENGTH-1:0] <= slave_word_d;
	     state <= RECV_STOP;
	  end
	
	RECV_STOP:
	  begin
	     //no stop bit -> some kind of transmission error
	     if (rx_d != 1'b1) error_d_o[NO_STOP_ERROR_BIT] <= 1'd1;
	     
	     //ok_valid_s_o <= 1'b1;
	     error_valid_s_o <= 1'b1;
	     state <= RECV_IDLE;
	  end
	
	default: state <= RECV_IDLE;

      endcase // case (state)
   end // always @ (posedge clk)

endmodule
`endif
