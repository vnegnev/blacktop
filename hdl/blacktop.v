
`ifndef _BLACKTOP_
 `define _BLACKTOP_

 `ifndef _DISPATCH_UNIT_
  `include "dispatch_unit.v"
 `endif

 `ifndef _RX_
  `include "rx.v"
 `endif

 `ifndef _TX_
  `include "tx.v"
 `endif

module blacktop(
			input 			      clk_c_i,
   
			input 			      data_present_s_i,
			output 			      busy_s_o,
			input [PACKET_LENGTH-1:0]     data_d_i,

			output reg 		      error_valid_s_o,
			output reg [ERROR_LENGTH-1:0] error_d_o,

			//transmit line
			output 			      tx_d_o,

			//receive line
			input 			      rx_d_i
			);

   //length of one word
   parameter PACKET_LENGTH = 48;
   
   // length of slave-side error word
   parameter SLAVE_ERROR_LENGTH = 10;

   // Length of concatenated RX + dispatch error word
   localparam ERROR_LENGTH = SLAVE_ERROR_LENGTH + 6;
   
   // length of RX-only error word
   localparam RX_ERROR_LENGTH = SLAVE_ERROR_LENGTH + 4;

   // length of dispatch-only error word
   localparam DISPATCH_ERROR_LENGTH = 2;

   // length of acknowledge timeout
   parameter ACK_WORD_TIMEOUT = 100;

   wire ack_s;
   wire req_s;
   wire [PACKET_LENGTH-1:0] tx_word_d;
   wire 		    send_ok_s;

   wire 		    ok_valid_s;
   wire [2-1:0] 	    ok_d;
   wire [RX_ERROR_LENGTH-1:0] rx_error_d;
   wire [DISPATCH_ERROR_LENGTH-1:0] dispatch_error_d;

   wire 		    rx_error_valid_s, dispatch_error_valid_s;

   initial begin
      error_valid_s_o = 0;
      error_d_o = 0;
   end
   
   // Error routing: forward errors from dispatch_unit and individual RXes
   always @(posedge clk_c_i) begin
      error_valid_s_o <= rx_error_valid_s || dispatch_error_valid_s;
      if (rx_error_valid_s) error_d_o[RX_ERROR_LENGTH-1:0] <= rx_error_d;
      if (dispatch_error_valid_s) error_d_o[ERROR_LENGTH-1:ERROR_LENGTH-DISPATCH_ERROR_LENGTH] <= dispatch_error_d;
   end

   dispatch_unit #(
		   .PACKET_LENGTH(PACKET_LENGTH),
		   .ACK_WORD_TIMEOUT(ACK_WORD_TIMEOUT)
		   )
   disp (
		       .clk_c_i(clk_c_i),
      
		       .ack_s_i(ack_s),
		       .req_s_o(req_s),
		       .tx_word_d_o(tx_word_d),
		       .send_ok_s_o(send_ok_s),
		       .busy_s_o(busy_s_o),
      
		       .ok_valid_s_i(ok_valid_s),
		       .ok_d_i(ok_d),

		       .error_valid_s_o(dispatch_error_valid_s),
		       .error_d_o(dispatch_error_d),
		       .valid_s_i(data_present_s_i),
		       .data_d_i(data_d_i)
		       );

   tx #(
	.PACKET_LENGTH(PACKET_LENGTH)
	) 
   trans(
	    .clk_c_i(clk_c_i),
      
	    .req_s_i(req_s),
	    .ack_s_o(ack_s),
	    .data_d_i(tx_word_d),
	    .send_ok_s_i(send_ok_s),
      
	    .tx_d_o(tx_d_o)
	    );
   
   rx #(
	.SLAVE_ERROR_LENGTH(SLAVE_ERROR_LENGTH)
	)
   recv(
	   .clk_c_i(clk_c_i),
      
	   .ok_valid_s_o(ok_valid_s),
	   .ok_d_o(ok_d),
      
	   .error_valid_s_o(rx_error_valid_s),
	   .error_d_o(rx_error_d),
      
	   .rx_d_i(rx_d_i)
	   );
   
endmodule
`endif
