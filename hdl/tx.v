//`timescale 1ns / 1ps

`ifndef _TX_
 `define _TX_
module tx(
	  input 		    clk_c_i,
   
	  //full handshake and incoming data
	  input 		    req_s_i,
	  output reg 		    ack_s_o,
	  input [PACKET_LENGTH-1:0] data_d_i,

	  input 		    send_ok_s_i,

	  //transmit line
	  output reg 		    tx_d_o
	  );
   
   //length of one word
   parameter PACKET_LENGTH = 48;

   //register that holds the word to be transmitted
   reg [PACKET_LENGTH-1:0] 	    reg_tx_d = 0;
   //register that holds the ongoing xor value
   reg 				    xor_s = 1'd0;
   
   //counter (multi-purpose)
   reg [6-1:0] 			    cnt_s = 0;
   

   //// FSM

   localparam IDLE = 'h1,
     SEND_OK = 'h2,
     SEND = 'h4,
     SEND_PARITY = 'h8,
     SEND_STOP = 'h10
		     ;
   reg [5-1:0] 			    state = IDLE;
   
   initial begin
      tx_d_o <= 1'b1;
      ack_s_o <= 1'b0;
   end

   always @(posedge clk_c_i) begin

      // defaults
      tx_d_o <= 1'b1;

      case(state)

	IDLE:
	  begin
	     if (req_s_i) begin
		ack_s_o <= 1'b1;
		
		//this signals the beginning of a word
		tx_d_o <= 1'b0;
		
		//save incoming data
		reg_tx_d <= data_d_i;
		
		//initialize counter and xor's
		cnt_s <= 0;
		xor_s <= 1'b0;
		state <= SEND_OK;
	     end
	  end
	
	SEND_OK:
	  begin
	     tx_d_o <= send_ok_s_i;
	     
	     //3 repeat
	     cnt_s <= cnt_s + 1;
	     if ( cnt_s==(3-1) ) begin
		cnt_s <= 0;
		state <= SEND;
	     end
	  end

	SEND:
	  begin
	     //left shift registers, transmit MSB and calculate xor
	     reg_tx_d <= reg_tx_d<<1;
	     tx_d_o <= reg_tx_d[PACKET_LENGTH-1];
	     xor_s <= xor_s ^ reg_tx_d[PACKET_LENGTH-1];
	     
	     //count "PACKET_LENGTH" steps
	     cnt_s <= cnt_s + 1;
	     if ( cnt_s==(PACKET_LENGTH-1) )
	       state <= SEND_PARITY;
	  end

	SEND_PARITY:
	  begin
	     //invert the xor to force a transition on all zero/one
	     tx_d_o <= !xor_s;
	     
	     //acknowledge completion, one cycle in advance
	     if (ack_s_o)
	       ack_s_o <= 1'b0;
	     
	     state <= SEND_STOP;
	  end

	SEND_STOP:
	  begin
	     //signals stop
	     tx_d_o <= 1'b1;
	     
	     state <= IDLE;
	  end			
	
	default: state <= IDLE;

      endcase // case (state)
   end // always @ (posedge clk)

endmodule
`endif //  `ifndef _TX_
