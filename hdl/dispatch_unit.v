//-----------------------------------------------------------------------------
// Title         : dispatch_unit
// Project       : zed_blacktop
//-----------------------------------------------------------------------------
// File          : dispatch_unit.v
// Author        :   edwind
// Created       : 04.10.2013
// Last modified : 04.10.2013
//-----------------------------------------------------------------------------
// Description :
// 
//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 04.10.2013 : created
//-----------------------------------------------------------------------------



`ifndef _DISPATCH_UNIT_
 `define _DISPATCH_UNIT_

module dispatch_unit(
		     input 			    clk_c_i,

		     //full handshake and incoming data
		     input 			    valid_s_i,
		     output reg 		    busy_s_o,
		     input [PACKET_LENGTH-1:0] 	    data_d_i, 

		     //to transmit line, full handshake
		     input 			    ack_s_i, // acknowledge from tx
		     output reg 		    req_s_o, // request to tx
		     output reg 		    send_ok_s_o, // 
		     output reg [PACKET_LENGTH-1:0] tx_word_d_o,

		     //from receiving line
		     input 			    ok_valid_s_i,
		     input [2-1:0] 		    ok_d_i,

		     //to error register
		     output reg 		    error_valid_s_o,
		     output reg [1:0] 		    error_d_o
		     );
   
   //length of one word
   parameter PACKET_LENGTH = 48;

   // timeout for ack word from slave
   parameter ACK_WORD_TIMEOUT = 100;

   // Error bits
   localparam TIMEOUT_BIT = 2;
   localparam RESEND_FAILED_BIT = 1;
   
   //to detect rising edge
   reg 						    ack_s_r = 1'b0;
   reg 						    valid_s_r = 1'b0;
   
   reg 						    two_resends_s = 1'b0;

   //registers that hold the words to be transmitted
   reg [PACKET_LENGTH-1:0] 			    pending_tx_word_d = 0;
   reg [PACKET_LENGTH-1:0] 			    old_tx_word_d = 0;
   
   // counter (multi-purpose)
   reg [8-1:0] 					    cnt_s = 0;
   

   //// FSM

   localparam IDLE = 'h1,
     SEND = 'h2,
     WAIT_FOR_OK = 'h4,
     SEND_AND_WAIT_FOR_OK = 'h8,
     WAIT_FOR_ACK = 'h10,
     RESEND = 'h20,
     RESEND_WAIT_FOR_OK = 'h40
		     ;
   reg [7-1:0] 					    state = IDLE;
   
   initial begin
      busy_s_o <= 1'b0;
      req_s_o <= 1'b0;
      send_ok_s_o <= 1'b1;
      tx_word_d_o <= 0;
      
      two_resends_s <= 1'b0;
      
      error_valid_s_o <= 1'b0;
      error_d_o <= 0;
   end

   always @(posedge clk_c_i) begin

      // defaults
      ack_s_r <= ack_s_i;
      valid_s_r <= valid_s_i;
      error_valid_s_o <= 0;
      error_d_o <= 2'd0;
      req_s_o <= 1'b0;
      
      case(state)			
	
	IDLE: begin
	   busy_s_o <= 1'b0;
	   send_ok_s_o <= 1'b1;
	   if (valid_s_i) begin			
	      req_s_o <= 1'b1;
	      tx_word_d_o <= data_d_i;
	      pending_tx_word_d <= data_d_i;
	      cnt_s <= 0;
	      state <= SEND;
	   end
	end
	
	SEND:
	  begin
	     // new incoming data -> accept, but block until last one was sent out successfully
	     if ( {valid_s_r,valid_s_i} == 2'b01 ) begin
		old_tx_word_d <= tx_word_d_o;
		pending_tx_word_d <= data_d_i;
		busy_s_o <= 1'b1;
	     end
	     
	     // finished sending out packet
	     if ( {ack_s_r,ack_s_i} == 2'b10 ) begin
		cnt_s <= 0;
		if ( valid_s_i ) begin
		   old_tx_word_d <= tx_word_d_o;
		   pending_tx_word_d <= data_d_i;
		   busy_s_o <= 1'b1;
		   req_s_o <= 1'b1;
		   tx_word_d_o <= data_d_i;
		   state <= SEND_AND_WAIT_FOR_OK;
		end else if( busy_s_o )	begin
		   req_s_o <= 1'b1;
		   tx_word_d_o <= pending_tx_word_d;
		   state <= SEND_AND_WAIT_FOR_OK;
		end else begin
		   state <= WAIT_FOR_OK;
		end
	     end
	  end
	
	WAIT_FOR_OK:
	  begin		 
	     // timeout
	     cnt_s <= cnt_s+1;
	     if (cnt_s == ACK_WORD_TIMEOUT) begin
		req_s_o <= 1'b1;
		tx_word_d_o <= pending_tx_word_d;
		busy_s_o <= 1'b1;
		cnt_s <= 0;
		state <= RESEND;
	     end
	     
	     // we received the ok word from the slave
	     if (ok_valid_s_i) begin
		// slave received packet successfully
		if ( ok_d_i==2'b11 ) begin
		   req_s_o <= 1'b0;
		   busy_s_o <= 1'b0;
		   state <= IDLE;
		   // error
		end else begin
		   req_s_o <= 1'b1;
		   tx_word_d_o <= pending_tx_word_d;
		   cnt_s <= 0;
		   state <= RESEND;
		end
	     end
	     
	     // start sending out new data (~back2back) UNLESS we receive an "error" message in this cycle
	     if ( valid_s_i && !(ok_valid_s_i && ok_d_i!=2'b11) ) begin
		old_tx_word_d <= pending_tx_word_d;
		pending_tx_word_d <= data_d_i;
		tx_word_d_o <= data_d_i;
		busy_s_o <= 1'b1;
		req_s_o <= 1'b1;
		state <= SEND_AND_WAIT_FOR_OK;
	     end
	     
	  end	
	
	SEND_AND_WAIT_FOR_OK:
	  begin
	     // finished sending out packet but did not receive an ok -> "timeout"
	     if ( {ack_s_r,ack_s_i} == 2'b10 ) begin
		req_s_o <= 1'b1;
		tx_word_d_o <= old_tx_word_d;
		send_ok_s_o <= 1'b0;
		two_resends_s <= 1'b1;
		state <= RESEND;
	     end
	     
	     // we received the ok word from the slave
	     // (possibly overwrites previous statements for "timeout")
	     if (ok_valid_s_i) begin
		req_s_o <= 1'b0;
		send_ok_s_o <= 1'b1;
		// slave received packet successfully
		// -> current packet becomes old packet and module doesn't block anymore
		if ( ok_d_i==2'b11 ) begin
		   old_tx_word_d <= pending_tx_word_d;
		   two_resends_s <= 1'b0;
		   busy_s_o <= 1'b0;
		   state <= SEND;
		   // error -> wait for transmission completion, then resend old packet
		end else begin
		   send_ok_s_o <= 1'b0;
		   state <= WAIT_FOR_ACK;
		end
	     end
	  end	
	
	WAIT_FOR_ACK:
	  begin
	     two_resends_s <= 1'b1;
	     // finished sending out packet
	     if ( {ack_s_r,ack_s_i} == 2'b10 ) begin
		tx_word_d_o <= old_tx_word_d;
		req_s_o <= 1'b1;
		state <= RESEND;
	     end
	  end	
	
	RESEND:
	  begin
	     busy_s_o <= 1'b1;
	     send_ok_s_o <= 1'b0;
	     
	     // finished sending out packet
	     if ( {ack_s_r,ack_s_i} == 2'b10 ) begin
		state <= RESEND_WAIT_FOR_OK;
	     end
	  end


	RESEND_WAIT_FOR_OK:
	  begin		 
	     // timeout
	     cnt_s <= cnt_s+1;
	     if (cnt_s == ACK_WORD_TIMEOUT) begin
		error_valid_s_o <= 1;
		error_d_o <= TIMEOUT_BIT;
		state <= IDLE;
	     end
	     
	     // we received the ok word from the slave
	     if (ok_valid_s_i) begin
		// slave acknowledged resend
		if ( ok_d_i==2'b10 ) begin
		   if ( two_resends_s ) begin
		      two_resends_s <= 1'b0;
		      busy_s_o <= 1'b0;
		      tx_word_d_o <= pending_tx_word_d;
		      req_s_o <= 1'b1;
		      state <= SEND;
		   end else
		     state <= IDLE;
		   
		   // error
		end else begin
		   error_valid_s_o <= 1;
		   error_d_o <= RESEND_FAILED_BIT;
		   
		   if ( two_resends_s ) begin // give up trying to resend and continue
		      two_resends_s <= 1'b0;
		      busy_s_o <= 1'b0;
		      tx_word_d_o <= pending_tx_word_d;
		      req_s_o <= 1'b1;
		      state <= SEND;
		   end else
		     state <= IDLE;
		end
	     end
	     
	     
	  end		  
	
	default: state <= IDLE;

      endcase // case (state)
   end // always @ (posedge clk)
   
   //assign send_ok_s_o = (state==ERR) ? 1'b0 : 1'b1;

endmodule
`endif //  `ifndef _DISPATCH_UNIT_
